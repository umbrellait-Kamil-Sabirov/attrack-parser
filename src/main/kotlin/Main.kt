import org.openqa.selenium.*
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import java.time.DayOfWeek
import java.time.Duration
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.time.temporal.TemporalAdjusters
import java.util.*


val works = mutableListOf<Work>()
val mainWorks = mutableListOf<Work>()
val filteredWorks = mutableListOf<Work>()

const val BCS_LOGIN = "SabirovKL"
const val BCS_PASSWORD = "KamilBCS1106"

fun cleanString(input: String): String {
    val prefix = "Jira("
    var result = if (input.contains(prefix)) {
        input.substringAfter(prefix).trimEnd { it == ' ' || it == '(' || it == ')' }
    } else {
        input
    }

    if (result.endsWith('ф')) {
        result = result.dropLast(1)
    }

    return result
}

fun getLastMonday(date: LocalDate): LocalDate {
    return date.minusDays((date.dayOfWeek.value - 1).toLong())
}

fun convertTime(time: String): String {
    val hourPart = if (time.contains(" ч ")) time.substringBefore(" ч ").trim() + "h" else ""
    val minutePart =
        if (time.contains(" мин")) time.substringAfterLast(" ч ").substringBefore(" мин").trim() + "m" else ""
    return (hourPart + minutePart).trim()
}


fun checkFriday(date: LocalDate): Pair<Boolean, Int> {
    val dayOfWeek = date.dayOfWeek
    println("dayOfWeek: $dayOfWeek")
    return if (dayOfWeek == DayOfWeek.FRIDAY) {
        Pair(true, 0)
    } else {
        val daysSinceFriday = (dayOfWeek.value - DayOfWeek.FRIDAY.value + 7) % 7
        Pair(false, daysSinceFriday)
    }
}

fun getLatestDate(date: LocalDate, count: Int): String {
    val a = date.minusDays((count).toLong())
    val formatter = DateTimeFormatter.ofPattern("dd.MM")
    println("Date: $a")
    val formatDate = formatter.format(a)
    if (count == 0) return "Сегодня [$formatDate]"
    if (count == 1) return "Вчера [$formatDate]"
    return formatter.format(a)
}

fun getWorkFromTable(wait: WebDriverWait, driver: WebDriver, work: Work) {
    println("getWorkFromTable word: $work")
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("grid")))

    val tasks = mutableListOf<Task>()
    val gridElement = driver.findElement(By.className("grid"))
    val projectElements =
        gridElement.findElements(By.xpath("./div[contains(@class, 'title') and .//div[@class='item__value' and text()=' Мир Инвестиций Mobile ']]"))

    for (projectElement in projectElements) {
        val taskElements =
            projectElement.findElements(By.xpath("following-sibling::div[contains(@class, 'ng-star-inserted')]"))

        var number = ""
        var description = ""
        var time = ""

        for (element in taskElements) {
            val text = element.text.trim()
            if (text.startsWith("[MB-")) {
                number = text.substringBefore("]").removePrefix("[")
                description = text.substringAfter("] ").substringAfterLast("- ")
            } else if (text.contains(" ч ") || text.contains(" мин")) {
                time = text
                tasks.add(Task(number, description, time))
            }
        }
    }

    for (i in works) {
        println(i)
        if (i.date == work.date) {
            println("in if")
            mainWorks.add(i.copy(task = tasks))
        }
    }
}

fun currentDate(dayWeek: String) {
    System.setProperty("webdriver.chrome.driver", "/opt/homebrew/bin/chromedriver")

    val options = ChromeOptions()
    options.addArguments("--remote-allow-origins=*")
    val driver: WebDriver = ChromeDriver(options)
    val actions = Actions(driver)

    try {
        driver.get("https://tracker.romdo.com/login")

        // Ожидание полной загрузки страницы с использованием JavaScript Executor
        val jsExecutor = driver as JavascriptExecutor
        WebDriverWait(
            driver,
            Duration.ofSeconds(10)
        ).until { jsExecutor.executeScript("return document.readyState") == "complete" }

        val wait = WebDriverWait(driver, Duration.ofSeconds(10))
        println("HTML source:")
        println(driver.pageSource)
        val googleSignInButton: WebElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@icon='https://assets-cdn.umbrellait.com/images/google-logo.svg']"))
        )
        googleSignInButton.click()

        wait.until(ExpectedConditions.urlContains("accounts.google.com"))

        val emailInput: WebElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='email' and @id='identifierId']"))
        )
        emailInput.sendKeys("kamil.sabirov@umbrellait.com")

        val nextButton: WebElement = wait.until(
            ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class, 'VfPpkd-LgbsSe') and span[text()='Далее']]"))
        )
        nextButton.click()

        val passwordInput: WebElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='password' and @name='Passwd']"))
        )
        passwordInput.sendKeys("umbrellaitpassword1")

        val passwordNextButton: WebElement = wait.until(
            ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class, 'VfPpkd-LgbsSe') and span[text()='Далее']]"))
        )
        passwordNextButton.click()

        val continueButton: WebElement = wait.until(
            ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class, 'VfPpkd-LgbsSe') and span[text()='Продолжить']]"))
        )
        continueButton.click()

        val closeButton: WebElement = wait.until(
            ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class, 'modal-close')]//div[text()='close']"))
        )
        closeButton.click()

        driver.get("https://tracker.romdo.com/report/by-projects")

        val zoneId = ZoneId.of("UTC+03:00")
        val currentDate = LocalDate.now(zoneId)
        println(currentDate)

        works.add(Work(date = getFormattedDateForDay(dayWeek)))


        println("Даты: $works размер ${works.size}")

        val resetFiltersButton: WebElement? = try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(@class, 'drop-settings') and text()='Сбросить фильтры']")))
        } catch (e: Exception) {
            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button.drop-settings")))
            } catch (e: Exception) {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(), 'Сбросить фильтры')]")))
            }
        }

        resetFiltersButton!!.click()


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'picked-range__title')]")))
        println("------")
        println(isTodayInDay(dayWeek))
        while (true) {
            if (mainWorks.size == 1) break
            val dateElement: WebElement = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'picked-range__title')]"))
            )
            val dateText = dateElement.text.trim()
            if (dateText == isTodayInDay(dayWeek)) {
                val reportButton: WebElement = wait.until(
                    ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class, 'color-yellow') and .//span[text()=' Сформировать отчёт ']]"))
                )
                reportButton.click()
                getWorkFromTable(wait = wait, driver, works[0])
            } else {
                val previousButton: WebElement = wait.until(
                    ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class, 'picked-range__navigation')]"))
                )
                previousButton.click()
            }

//            val isCurrentDate = if (works[index].date.length > 5) {
//                val date = works[index].date.substringBefore("[").trim()
//                println("dateFiltred: $date HTML date: $dateText")
//                dateText == date
//            } else {
//                println("works[index].date: ${works[index].date} HTML date: $dateText")
//                dateText == works[index].date
//            }

//            println("isCurrentDate: $isCurrentDate")
//            if (isCurrentDate) {
//                println("if block")
//                val reportButton: WebElement = wait.until(
//                    ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class, 'color-yellow') and .//span[text()=' Сформировать отчёт ']]"))
//                )
//                reportButton.click()
//                Thread.sleep(2000)
//                getWorkFromTable(wait = wait, driver, works[index])
//                Thread.sleep(1000)
//
//                val previousButton: WebElement = wait.until(
//                    ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class, 'picked-range__navigation')]"))
//                )
//                previousButton.click()
//                index++
//            } else {
//                index = 0
//                val previousButton: WebElement = wait.until(
//                    ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class, 'picked-range__navigation')]"))
//                )
//                previousButton.click()
//                // Задержка перед проверкой следующей даты
//                Thread.sleep(1000)
//            }
        }

        for (i in mainWorks) {
            val filteredDate = if (i.date.length > 5) i.date.substringAfter("[").substringBefore("]") else i.date
            val updatedTasks = i.task.map { task ->
                val lastSymbol = task.description.substringAfter("(").substringBefore(")").last()
                val newDescription = if (lastSymbol == 'ф') {
                    task.description.dropLast(1)
                } else task.description

                val type = when (lastSymbol) {
                    'ф' -> TaskType.Feature
                    else -> TaskType.Bug
                }
                val newType = if ("108385" in task.number) TaskType.Meet else type
                task.copy(type = newType, description = newDescription)
            }
            filteredWorks.add(i.copy(date = filteredDate, task = updatedTasks))
        }


        driver.get("https://jira.bcs.ru/login.jsp")
        val usernameInput: WebElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.id("login-form-username"))
        )
        usernameInput.sendKeys(BCS_LOGIN)

        val passwordInputBcs: WebElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.id("login-form-password"))
        )
        passwordInputBcs.sendKeys(BCS_PASSWORD)

        val loginButton: WebElement = wait.until(
            ExpectedConditions.elementToBeClickable(By.id("login-form-submit"))
        )
        loginButton.click()

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("body")))

        driver.get("https://jira.bcs.ru/secure/Tempo.jspa#/my-work/week?type=LIST")
        val reversedList = filteredWorks.reversed()
        println("reversedList: $reversedList")
        for (work in reversedList) {
            for (task in work.task) {
                val dateParts = work.date.split(".")
                val dateFormatted = "2024-${dateParts[1]}-${dateParts[0]}"

                val dayBlock: WebElement = wait.until(
                    ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='$dateFormatted' and @name='calendarListViewDay']"))
                )

                val workButton: WebElement =
                    dayBlock.findElement(By.xpath(".//div[@name='tempoCalendarLogWork' and text()='Работа']"))
                jsExecutor.executeScript("arguments[0].click();", workButton)

                val issueInput: WebElement =
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("issuePickerInput")))
                issueInput.sendKeys(task.number)


                val issueItem: WebElement = wait.until(
                    ExpectedConditions.elementToBeClickable(
                        By.xpath("//div[@id='issueDropdown']//div[contains(@class, 'sc-jfmDQi kMWraI')]/div[text()='" + task.number + "']")
                    )
                )
                issueItem.click()

                val descriptionInput: WebElement =
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//textarea[@placeholder='Описание']")))
                descriptionInput.sendKeys(cleanString(task.description))
                val timeInput: WebElement =
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='0h']")))
                timeInput.sendKeys(convertTime(task.time))

//                val selectIcon: WebElement = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='sc-iIPllB kVWksa']")))
//                selectIcon.click()

//                val dropdownToggle: WebElement = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='sc-iIPllB kVWksa']")))
//                dropdownToggle.click()

                val dropdownToggle: WebElement =
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='sc-iIPllB kVWksa']")))

                wait.until(ExpectedConditions.elementToBeClickable(dropdownToggle))

                dropdownToggle.click()


                for (i in 0..task.type.ordinal) {
                    actions.sendKeys(Keys.ARROW_DOWN).perform()
                    Thread.sleep(500)
                }
                actions.sendKeys(Keys.ENTER).perform()


                val work: WebElement =
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[.//span[contains(text(), 'Работа')]]")))

                work.click()

                Thread.sleep(3500)
            }
        }

    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        driver.quit()
    }
}

fun allWeek() {
    System.setProperty("webdriver.chrome.driver", "/opt/homebrew/bin/chromedriver")

    // Инициализация драйвера
    val options = ChromeOptions()
    options.addArguments("--remote-allow-origins=*")
    val driver: WebDriver = ChromeDriver(options)
    val actions = Actions(driver)

    try {
        driver.get("https://tracker.romdo.com/login")

        val jsExecutor = driver as JavascriptExecutor
        WebDriverWait(
            driver,
            Duration.ofSeconds(30)
        ).until { jsExecutor.executeScript("return document.readyState") == "complete" }

        val wait = WebDriverWait(driver, Duration.ofSeconds(30))
        println("HTML source:")
        println(driver.pageSource)
        val googleSignInButton: WebElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@icon='https://assets-cdn.umbrellait.com/images/google-logo.svg']"))
        )
        googleSignInButton.click()

        wait.until(ExpectedConditions.urlContains("accounts.google.com"))

        val emailInput: WebElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='email' and @id='identifierId']"))
        )
        emailInput.sendKeys("kamil.sabirov@umbrellait.com")

        val nextButton: WebElement = wait.until(
            ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class, 'VfPpkd-LgbsSe') and span[text()='Далее']]"))
        )
        nextButton.click()

        val passwordInput: WebElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='password' and @name='Passwd']"))
        )
        passwordInput.sendKeys("umbrellaitpassword1")

        val passwordNextButton: WebElement = wait.until(
            ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class, 'VfPpkd-LgbsSe') and span[text()='Далее']]"))
        )
        passwordNextButton.click()

        val continueButton: WebElement = wait.until(
            ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class, 'VfPpkd-LgbsSe') and span[text()='Продолжить']]"))
        )
        continueButton.click()

        val closeButton: WebElement = wait.until(
            ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class, 'modal-close')]//div[text()='close']"))
        )
        closeButton.click()

        driver.get("https://tracker.romdo.com/report/by-projects")

        val zoneId = ZoneId.of("UTC+03:00")
        val currentDate = LocalDate.now(zoneId)
        println(currentDate)
        val isFriday = checkFriday(currentDate)
        if (isFriday.first) {
            for (i in 0..4) {
                works.add(Work(date = getLatestDate(date = currentDate, count = i)))
            }
        } else {
            for (i in 0..4) {
                works.add(Work(date = getLatestDate(date = currentDate, count = isFriday.second + i)))
            }
        }

        println("Даты: $works размер ${works.size}")

        val resetFiltersButton: WebElement? = try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(@class, 'drop-settings') and text()='Сбросить фильтры']")))
        } catch (e: Exception) {
            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button.drop-settings")))
            } catch (e: Exception) {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(), 'Сбросить фильтры')]")))
            }
        }

        resetFiltersButton!!.click()


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'picked-range__title')]")))

        var index = 0
        while (true) {
            if (mainWorks.size == 5) break
            val dateElement: WebElement = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'picked-range__title')]"))
            )
            val dateText = dateElement.text.trim()
            val isCurrentDate = if (works[index].date.length > 5) {
                val date = works[index].date.substringBefore("[").trim()
                println("dateFiltred: $date HTML date: $dateText")
                dateText == date
            } else {
                println("works[index].date: ${works[index].date} HTML date: $dateText")
                dateText == works[index].date
            }

            println("isCurrentDate: $isCurrentDate")
            if (isCurrentDate) {
                println("if block")
                val reportButton: WebElement = wait.until(
                    ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class, 'color-yellow') and .//span[text()=' Сформировать отчёт ']]"))
                )
                reportButton.click()
                Thread.sleep(2000)
                getWorkFromTable(wait = wait, driver, works[index])
                Thread.sleep(1000)

                val previousButton: WebElement = wait.until(
                    ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class, 'picked-range__navigation')]"))
                )
                previousButton.click()
                index++
            } else {
                index = 0
                val previousButton: WebElement = wait.until(
                    ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class, 'picked-range__navigation')]"))
                )
                previousButton.click()
                Thread.sleep(1000)
            }
        }

        for (i in mainWorks) {
            val filteredDate = if (i.date.length > 5) i.date.substringAfter("[").substringBefore("]") else i.date
            val updatedTasks = i.task.map { task ->
                val lastSymbol = task.description.substringAfter("(").substringBefore(")").last()
                val newDescription = if (lastSymbol == 'ф') {
                    task.description.dropLast(1)
                } else task.description

                val type = when (lastSymbol) {
                    'ф' -> TaskType.Feature
                    else -> TaskType.Bug
                }
                val newType = if ("108385" in task.number) TaskType.Meet else type
                task.copy(type = newType, description = newDescription)
            }
            filteredWorks.add(i.copy(date = filteredDate, task = updatedTasks))
        }


        driver.get("https://jira.bcs.ru/login.jsp")
        val usernameInput: WebElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.id("login-form-username"))
        )
        usernameInput.sendKeys(BCS_LOGIN)

        val passwordInputBcs: WebElement = wait.until(
            ExpectedConditions.visibilityOfElementLocated(By.id("login-form-password"))
        )
        passwordInputBcs.sendKeys(BCS_PASSWORD)

        val loginButton: WebElement = wait.until(
            ExpectedConditions.elementToBeClickable(By.id("login-form-submit"))
        )
        loginButton.click()

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("body")))

        driver.get("https://jira.bcs.ru/secure/Tempo.jspa#/my-work/week?type=LIST")
        val reversedList = filteredWorks.reversed()
        println("reversedList: $reversedList")
        for (work in reversedList) {
            for (task in work.task) {
                val dateParts = work.date.split(".")
                val dateFormatted = "2024-${dateParts[1]}-${dateParts[0]}"

                val dayBlock: WebElement = wait.until(
                    ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='$dateFormatted' and @name='calendarListViewDay']"))
                )

                val workButton: WebElement =
                    dayBlock.findElement(By.xpath(".//div[@name='tempoCalendarLogWork' and text()='Работа']"))
                jsExecutor.executeScript("arguments[0].click();", workButton)

                val issueInput: WebElement =
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("issuePickerInput")))
                issueInput.sendKeys(task.number)


                val issueItem: WebElement = wait.until(
                    ExpectedConditions.elementToBeClickable(
                        By.xpath("//div[@id='issueDropdown']//div[contains(@class, 'sc-jfmDQi kMWraI')]/div[text()='" + task.number + "']")
                    )
                )
                issueItem.click()

                val descriptionInput: WebElement =
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//textarea[@placeholder='Описание']")))
                descriptionInput.sendKeys(cleanString(task.description))
                val timeInput: WebElement =
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='0h']")))
                timeInput.sendKeys(convertTime(task.time))

//                val selectIcon: WebElement = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='sc-iIPllB kVWksa']")))
//                selectIcon.click()

//                val dropdownToggle: WebElement = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='sc-iIPllB kVWksa']")))
//                dropdownToggle.click()

                val dropdownToggle: WebElement =
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='sc-iIPllB kVWksa']")))

                wait.until(ExpectedConditions.elementToBeClickable(dropdownToggle))

                dropdownToggle.click()


                for (i in 0..task.type.ordinal) { // Предполагаем, что "Meetings" - третий элемент в списке
                    actions.sendKeys(Keys.ARROW_DOWN).perform()
                    Thread.sleep(500)
                }
                actions.sendKeys(Keys.ENTER).perform()


                val work: WebElement =
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[.//span[contains(text(), 'Работа')]]")))

                work.click()

                Thread.sleep(3500)
            }
        }

    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        driver.quit()
    }
}

fun main() {
    currentDate("ЧТ")
    //allWeek()
}

fun isTodayInDay(inputDay: String): String {
    val currentDate = LocalDate.now()
    val currentDayOfWeek = currentDate.dayOfWeek

    val daysMap = mapOf(
        "ПН" to DayOfWeek.MONDAY,
        "ВТ" to DayOfWeek.TUESDAY,
        "СР" to DayOfWeek.WEDNESDAY,
        "ЧТ" to DayOfWeek.THURSDAY,
        "ПТ" to DayOfWeek.FRIDAY,
        "СБ" to DayOfWeek.SATURDAY,
        "ВС" to DayOfWeek.SUNDAY
    )

    val inputDayOfWeek = daysMap[inputDay] ?: return "Неверный входной день"

    return when {
        currentDayOfWeek == inputDayOfWeek -> "Сегодня"
        currentDayOfWeek.minus(1) == inputDayOfWeek -> "Вчера"
        else -> {
            val lastDay = currentDate.with(TemporalAdjusters.previous(inputDayOfWeek))
            lastDay.format(DateTimeFormatter.ofPattern("dd.MM"))
        }
    }
}


fun getFormattedDateForDay(inputDay: String): String {
    val daysMap = mapOf(
        "ПН" to DayOfWeek.MONDAY,
        "ВТ" to DayOfWeek.TUESDAY,
        "СР" to DayOfWeek.WEDNESDAY,
        "ЧТ" to DayOfWeek.THURSDAY,
        "ПТ" to DayOfWeek.FRIDAY,
        "СБ" to DayOfWeek.SATURDAY,
        "ВС" to DayOfWeek.SUNDAY
    )

    val inputDayOfWeek = daysMap[inputDay] ?: return "Неверный входной день"

    val currentDate = LocalDate.now()

    val lastDay = currentDate.with(TemporalAdjusters.previousOrSame(inputDayOfWeek))

    val formatter = DateTimeFormatter.ofPattern("dd.MM")
    return lastDay.format(formatter)
}

data class Work(
    val date: String,
    val task: List<Task> = emptyList()
)

data class Task(
    val number: String = "",
    val description: String = "",
    val time: String = "",
    val type: TaskType = TaskType.Feature
)

enum class TaskType(description: String) {
    Feature("Change"),
    Bug("Support (Run)"),
    Meet("Meetings")
}
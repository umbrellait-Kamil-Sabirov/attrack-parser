plugins {
    kotlin("jvm") version "1.9.23"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
    implementation("org.seleniumhq.selenium:selenium-java:4.1.0")
    implementation("org.seleniumhq.selenium:selenium-chrome-driver:4.1.0")
    implementation("io.github.bonigarcia:webdrivermanager:5.3.1")
    implementation("org.seleniumhq.selenium:selenium-java:4.1.0")
    implementation("org.seleniumhq.selenium:selenium-chrome-driver:4.1.0")
    implementation("io.github.bonigarcia:webdrivermanager:5.3.1")
    implementation("org.slf4j:slf4j-api:2.0.3")
    implementation("org.slf4j:slf4j-simple:2.0.3")

}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(17)
}